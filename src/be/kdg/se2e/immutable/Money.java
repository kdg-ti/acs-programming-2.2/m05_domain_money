package be.kdg.se2e.immutable;

import java.util.Currency;
import java.util.Objects;

// final class so there can be no mutable subclasses
public final class Money {
	//final attributes can only be set by constructor
	private final double amount;
	private final Currency currency;

	public Money(double amount, Currency currency) {
		this.amount = amount;
		this.currency = currency;
	}

	// no setters
	public double getAmount() {
		return amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Money)) return false;
		Money that = (Money) o;
		return Double.compare(that.amount, amount) == 0
			&& currency.equals(that.currency);
	}

	@Override
	public int hashCode() {
		return Objects.hash(amount, currency);
	}
}