package be.kdg.se2e.record;

import java.util.Currency;

public record Money(double amount, Currency currency) {
}
